var express = require('express');
var router = express.Router();

var mysql = require('mysql');

var db = mysql.createPool({
  host: 'localhost',
  user: 'rout',
  password: '',
  database: 'RestAPI_db'
});


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.get('/testdb', function(req, res, next){
  if (db != null) {
    res.send('database working.. ');
  } else {
    res.send('database not working!!!!!')
  }
});

module.exports = router;
