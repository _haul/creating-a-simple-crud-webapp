var express = require('express');
var router = express.Router();
var mysql = require('mysql');

var db = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'RestAPI_db'
});

/* GET employees listing. */
router.get('/',(req, res, next)=> {
  db.query("SELECT * FROM `employees`", (err, rs)=> {
    res.render('employees.ejs', { employees : rs });
  });
});

//get one employee
router.get('/one/:emp_id', (req, res, next)=>{
db.query('SELECT *FROM employees WHERE emp_id = ?', req.params.emp_id, (err, rs)=> {
    res.render('oneEmployee', {employees: rs[0]})
  })
});

//get the add employees form
router.get('/add', (req, res, next)=> {
  res.render('addEmployee');
});

//add an employee
router.post('/add', (req, res, next)=> {
  db.query('INSERT INTO employees SET ?', req.body, (err, rs)=> {
    res.redirect('/employees'); 
   });
});

//delete an employee
router.get('/delete/:emp_id', (req, res, next)=> {
  db.query('DELETE FROM employees WHERE emp_id = ?', req.params.emp_id, (err, rs)=> {
    res.redirect('/employees');
  });
});

//get the edit form
router.get('/edit/:emp_id', (req, res, next)=> {
  db.query("SELECT * FROM employees WHERE emp_id = ?", req.params.emp_id, (err, rs)=> {
    res.render('editEmployees.ejs', {employees: rs[0]});
  });
});

//edit an employee informations
router.post('/edit/:emp_id', (req, res, next)=>{
  var newInfo = [
    req.body,
    req.params.emp_id
  ]
db.query('UPDATE employees SET ? WHERE emp_id = ?',newInfo, (err, rs)=> {
    res.redirect('/employees');
  })
});

module.exports = router;
